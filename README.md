# Árvore de Expressão

## Requisitos

Para resolver esse problema você deve conhecer:

* Árvores binárias
* Notação pós-fixa
* Recursão

## Parte 1

Neste trabalho você deve criar um gerador de árvore de expressão. Este gerador recebe uma expressão pós-fixa e a partir dela constrói a árvore que a representa.

Você encontrará mais sobre árvores de expressão [aqui](https://en.wikipedia.org/wiki/Binary_expression_tree).

### Exemplo

A expressão pós-fixa

```
3 5 9 + 2 * +
```

gera a seguinte árvore:

![arvore](http://www.geeksforgeeks.org/wp-content/uploads/expressiontre.png)


## Parte 2

Para verificar se a árvore foi gerada corretamente, vamos criar uma forma de visualizá-la. Para isso imprimiremos a árvore de expressão gerada de forma que cada nível fique em uma linha da impressão. Dessa forma a árvore gerada na Parte 1 terá o seguinte formato de impressão:

```
nivel 0: +
nivel 1: 3*
nivel 2: +2
nivel 3: 59
```

## Parte 3

Tenso montado a árvore de expressão com sucesso, resolva a expressão matemática que a árvore representa e imprima o resultado. Por exemplo, para a árvore da Parte 1, você deve imprimir:

```
31
```

## Testes

Nas partes 2 e 3 do trabalho você pode utilizar os arquivos de teste para verificar se a implementação está correta.
Para isso você deve seguir os formatos de entrada e saída descritos nas respectivas seções.

#### Entrada

A primeira linha do arquivo de entrada consiste em um valor inteiro *n*(1<=*n*<10) que contém o número de casos de teste. As *n* linhas seguintes conterão expressões matemáticas, uma por linha, pós-fixadas que deverão ser processadas pelo seu programa. Uma expressão contém até 100 itens, onde um item pode ser um valor float ou um operador
(+, -, *, /).

##### Exemplo

```
4
3 5 9 + 2 * +
2 2 + 5 3 * -
10 3 * 20 +
```

#### Saída Parte 2

#### Saída Parte 3

A saída do código consistem em *n* linhas com valores de ponto flutuante que representam o resultado das *n* expressões de entrada, com precisão de 2 dígitos.

##### Exemplo

```
31
-11
50
```
