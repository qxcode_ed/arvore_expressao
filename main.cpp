#include <iostream>
#include <sstream>
#include <vector>
#include <stack>
#include <cstdlib>
#include <queue>

using namespace std;

struct Node
{
    bool isLeaf;
    char op;
    float value;
    Node *left;
    Node *right;
};

Node *root = nullptr;

void percorrerPorNivel(
        Node *node,
        vector<string> &niveis,
        int nivel = 0)
{
    if (node == nullptr)
        return;

    if (int(niveis.size()) < nivel+1)
        niveis.push_back("");

    if (node->isLeaf)
        niveis[nivel] += std::to_string(node->value);
    else
        niveis[nivel] += node->op;

    percorrerPorNivel(node->left, niveis, nivel+1);
    percorrerPorNivel(node->right, niveis, nivel+1);
}

void drawn(Node *node, int nivel = 0)
{
    if (node == nullptr)
        return;

    drawn(node->right, nivel+1);

    cout << string(nivel * 4, ' ');
    if (node->isLeaf)
        cout << node->value << endl;
    else
        cout << node->op << endl;

    drawn(node->left, nivel+1);
}

float operate(char op, float a, float b)
{
    switch (op) {
    case '+':
        return a + b;
        break;
    case '-':
        return a - b;
        break;
    case '*':
        return a * b;
        break;
    case '/':
        return a / b;
        break;
    default:
        return 0;
        break;
    }
}



float solve(Node * no)
{
    if(no->isLeaf)
        return no->value;
    return operate(no->op, solve(no->left), solve(no->right));
}

float solveView(Node *node)
{

    if (node->isLeaf)
    {
        return node->value;
    }

    cout << endl;
    drawn(root);
    cout << "___________________" << endl;

    float a = solveView(node->left);
    float b = solveView(node->right);
    node->isLeaf = true;
    node->value = operate(node->op, a, b);
    delete node->left;
    node->left = nullptr;
    delete node->right;
    node->right = nullptr;

    cout << endl;
    drawn(root);
    cout << "___________________" << endl;

    return node->value;
}



int main()
{
    string temp;
    string exp = "10 3 * 20 +";
    stringstream ss;
    vector<Node*> vTree;
    stack<Node*> pilha;

    ss << exp;
    while (ss >> temp)
    {
        Node *node;
        switch (temp[0]) {
        case '+':
            node = new Node{false, '+', 0, nullptr, nullptr};
            break;
        case '-':
            node = new Node{false, '-', 0, nullptr, nullptr};
            break;
        case '*':
            node = new Node{false, '*', 0, nullptr, nullptr};
            break;
        case '/':
            node = new Node{false, '/', 0, nullptr, nullptr};
            break;
        default:
            node = new Node{true, ' ', stof(temp), nullptr, nullptr};
            break;
        }
        vTree.push_back(node);

    }


    for (Node* node : vTree)
    {
        if (node->isLeaf)
        {
            pilha.push(node);
        }
        else
        {
            if (int(pilha.size()) < 2)
                throw(-1);
            node->right = pilha.top();
            pilha.pop();
            node->left = pilha.top();
            pilha.pop();
            pilha.push(node);
        }
        
    }

    drawn(pilha.top());
    cout << endl;
    root = pilha.top();

    /*
    queue<Node*> fila;
    fila.push(pilha.top());
    while (!fila.empty())
    {
        Node * node = fila.front();

        if (node != nullptr)
        {
            fila.push(node->left);
            fila.push(node->right);

            if (node->isLeaf)
                cout << node->value << " ";
            else
                cout << node->op << " ";
        }

        fila.pop();
    }
    cout << endl;
    */

    vector<string> niveis;

    percorrerPorNivel(root, niveis);

    for (string &nv : niveis)
        cout << nv << endl;

    return 0;
}
